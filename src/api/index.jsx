import { apiClassList } from "./modules/classRoom/index"
import { studentDetail } from "./modules/student/index"
import { apiRouterList } from "./modules/login/index"

import { routerDetail } from "./modules/user/index"
export { apiClassList, studentDetail, apiRouterList, routerDetail }
