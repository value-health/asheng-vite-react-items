import request from "utils/request"

export const studentDetail = (data) =>
  request({
    path: "/studentDetail",
    data,
    method: "post"
  }) //登录
