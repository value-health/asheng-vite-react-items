import { configureStore } from "@reduxjs/toolkit"

import cart from "./carts"

const store = configureStore({
  reducer: cart.reducer
})

export default store
