import { createSlice } from "@reduxjs/toolkit" //createSlice创建方法

const cart = createSlice({
  name: "cart",
  initialState: {
    //存放数据的位置
    cart: [],
    routers: []
  },
  reducers: {
    //执行story中方法，操作数据的位置

    Add(state, { payload: id }) {
      state.cart.push(id)
    },
    SetRouters(state, { payload: routers }) {
      state.routers = routers
    }
  }
})

export const { Add, SetRouters } = cart.actions
//需要将我们的方法进行暴露，之后在使用时可以调用

export default cart //暴露我们的cart，在后面story的index.js使用
