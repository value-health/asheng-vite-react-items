import { adminRouter } from "@/router/index"
// 引入
import { useRoutes } from "react-router-dom"

const App = () => {
  let element = useRoutes(adminRouter)
  return <div>{element}</div>
}

export default App
