import { Button, Result } from "antd"
const App = () => (
  <Result
    status="404"
    title="404"
    subTitle="对不起，您访问的页面不存在"
    extra={<Button type="primary">Back Home</Button>}
  />
)
export default App
