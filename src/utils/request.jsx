import axios from "axios"
import qs from "qs"
import { addPending, removePending } from "@/utils/axios"
import { message, Modal } from "antd"

const isMode = import.meta.env.MODE === "development"
const token = JSON.parse(sessionStorage.getItem("token")) || ""
let baseURL = isMode ? "http://localhost:3000" : "http://localhost:3666"

const service = axios.create({
  baseURL: baseURL,
  timeout: 5000,
  //   withCredentials: true,
  // responseType: 'json',
  headers: {
    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
  }
})

service.interceptors.request.use(
  (config) => {
    !config.repeatRequest && addPending(config)
    config.data = qs.stringify(config.data)
    if (token) {
      config.headers["token"] = token
    }
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)
service.interceptors.response.use(
  (response) => {
    removePending(response.config)
    const res = response.data
    // 处理异常的情况
    if (res.code !== 200) {
      message.error("请求失败")
      // // 403:非法的token; 401:Token 过期了;
      if (res.code === 403 || res.code === 401) {
        // 清除token信息并跳转到登录页面
        sessionStorage.clear()
        Modal.confirm({
          title: "你已被登出，可以取消继续留在该页面，或者重新登录",
          okText: "重新登录",
          cancelText: "取消",
          onOk: () => {
            window.location.href = "/login"
          },
          onCancel: () => {
            window.location.reload()
          }
        })
        return Promise.reject(res)
      }
      return Promise.reject("error")
    } else {
      // 默认只返回data，不返回状态码和message
      // 通过 meta 中的 responseAll 配置来取决后台是否返回所有数据(包括状态码，message和data)
      const isBackAll = response.config.meta && response.config.meta.responseAll
      return isBackAll ? res : res.data
    }
  },
  (error) => {
    // 400-599
    if (error.response.status) {
      switch (error.response.status) {
        case 401:
          // 未登录
          message.info("用户未登录")
          break
        case 403:
          message.info("登录过期，请重新登录")
          sessionStorage.removeItem("token")
          setTimeout(() => {
            //登录页跳转
            window.location.href = "/login"
          }, 1000)
          break
        case 404:
          // 404请求不存在
          message.info("404 NOT FOUND")
          break
        case 405:
          // 请求的方法禁用
          break
        case 500:
          message.info("服务器暂时不可用")
          break
        case 501 || 502 || 503 || 504:
          // 503 服务器暂时不可用
          message.info("服务器暂时不可用")
          break
        case 505:
          alert("请求的资源不存在")
          break
        default:
          message.info("服务器不支持请求中所使用的 HTTP 协议版本")
      }
    }
    error.config && removePending(error.config)
  }
)

const request = ({ url, path = "", params = {}, data, method = "POST", ...rest }) => {
  return new Promise((resole, reject) => {
    return axios({
      method,
      url: url || baseURL + path,
      data: {
        ...data,
        ...params
      },
      ...rest
    })
      .then((res) => {
        // console.log(res);
        resole(res && res.data)
      })
      .catch((error) => {
        reject(error)
      })
  })
}

export default request
