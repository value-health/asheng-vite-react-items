import axios from "axios"
const pendingMap = new Map()

/**
 * 生成每个请求唯一的键
 * @param {*} config
 * @returns string
 */
// 功能：根据传入的config参数，获取一个唯一的key
// 参数：config：包含url，method，params，data的参数对象
// 返回值：一个唯一的key
function getPendingKey(config) {
  let { url, method, params, data } = config
  if (typeof data === "string") data = JSON.parse(data) // response里面返回的config.data是个字符串对象
  return [url, method, JSON.stringify(params), JSON.stringify(data)].join("&")
}

/**
 * 储存每个请求唯一值, 也就是cancel()方法, 用于取消请求
 * @param {*} config
 */
// 函数addPending，用于添加一个取消令牌
function addPending(config) {
  // 获取取消令牌的键
  const pendingKey = getPendingKey(config)
  // 设置取消令牌
  config.cancelToken =
    config.cancelToken ||
    new axios.CancelToken((cancel) => {
      // 如果pendingMap中没有该键，则添加该键
      if (!pendingMap.has(pendingKey)) {
        pendingMap.set(pendingKey, cancel)
      }
    })
}
/**
 * 删除重复的请求
 * @param {*} config
 */
// 函数removePending，用于移除pendingMap中的pendingKey
function removePending(config) {
  // 获取pendingKey
  const pendingKey = getPendingKey(config)
  // 判断pendingMap中是否有pendingKey
  if (pendingMap.has(pendingKey)) {
    // 获取pendingMap中的cancelToken
    const cancelToken = pendingMap.get(pendingKey)
    // 调用cancelToken函数，传入pendingKey
    cancelToken(pendingKey)
    // 删除pendingMap中的pendingKey
    pendingMap.delete(pendingKey)
  }
}

export { getPendingKey, addPending, removePending }
