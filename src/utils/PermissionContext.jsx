import { createContext } from "react"

const PermissionContext = createContext()

export const PermissionContextProvider = PermissionContext.Provider
export const PermissionContextConsumer = PermissionContext.Consumer
export default PermissionContext
