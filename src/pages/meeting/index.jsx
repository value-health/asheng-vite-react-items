import { useState, useEffect } from "react"
import { apiClassList } from "@/api/index"
const Meeting = () => {
  const [data, setData] = useState([])
  useEffect(() => {
    getDate()
  }, [data])
  const getDate = async () => {
    let list = await apiClassList()
    setData(() => list)
  }
  return <div>This is Meeting</div>
}

export default Meeting
