import { lazy } from "react"
import { Navigate } from "react-router-dom"
import Login from "@/pages/login/index"
import Register from "@/pages/register/index"
import Admin from "@/pages/admin/index"
import Student from "@/pages/student/index"

const staticRouter = [
  {
    path: "/login",
    element: <Login />
  },
  {
    path: "/register",
    element: <Register />
  }
]

const adminRouter = [
  {
    path: "/",
    element: <Navigate to="/admin" />
  },
  {
    path: "/admin",
    name: "工作台",
    withPermission: false,
    element: <Admin />
  },
  {
    path: "/student",
    name: "学生模块",
    withPermission: false,
    element: <Student />
  },
  {
    path: "/teacher",
    name: "老师模块",
    withPermission: true,
    permission: "teacher", // 访问页面的权限名称
    element: lazy(() => import("@/pages/teacher/index"))
  },
  {
    path: "/class",
    name: "班级模块",
    withPermission: true,
    element: lazy(() => import("@/pages/class/index"))
  },
  {
    path: "/classroom",
    name: "教室模块",
    withPermission: false,
    element: lazy(() => import("@/pages/classroom/index"))
  },
  {
    path: "/meeting",
    name: "会议模块",
    withPermission: false,
    element: lazy(() => import("@/pages/meeting/index"))
  },
  {
    path: "/401",
    name: "401",
    withPermission: false,
    element: lazy(() => import("@/components/Result/401/index"))
  }
]

export { staticRouter, adminRouter }
