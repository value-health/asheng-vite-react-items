import { defineConfig } from "vite"
import react from "@vitejs/plugin-react"
import path from "path"
import { fileURLToPath } from "url"
import viteEslint from "vite-plugin-eslint"
const __dirname = path.dirname(fileURLToPath(import.meta.url))

// https://vitejs.dev/config/
export default () => {
  const alias = [
    { find: "@", replacement: path.resolve(__dirname, "src") },
    {
      find: "components",
      replacement: path.resolve(__dirname, "src/components")
    },
    { find: "config", replacement: path.resolve(__dirname, "src/config") },
    { find: "store", replacement: path.resolve(__dirname, "src/store") },
    { find: "utils", replacement: path.resolve(__dirname, "src/utils") },
    { find: "api", replacement: path.resolve(__dirname, "src/api") },
    { find: /^~/, replacement: "" }
  ]
  return defineConfig({
    plugins: [
      react(),
      viteEslint({
        failOnError: false
      })
    ],
    resolve: {
      alias
    }
  })
}
